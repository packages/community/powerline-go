# Maintainer: Mark Wagie <mark at manjaro dot org>

pkgname=powerline-go
pkgver=1.25
pkgrel=1
pkgdesc="A Powerline like prompt for Bash, ZSH and Fish"
arch=('x86_64' 'aarch64')
url="https://github.com/justjanne/powerline-go"
license=('GPL-3.0-or-later')
depends=('glibc')
makedepends=('go' 'git')
optdepends=('powerline-fonts: patched fonts for powerline')
source=("$pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz")
sha256sums=('64cb194bbf08536320d0f4c24ef9524fdf486f579147cacdb0b6dc0afc1134e2')

prepare() {
  cd "$pkgname-$pkgver"
  export GOPATH="$srcdir/gopath"
  go mod download -x

  mkdir -p build
}

build() {
  cd "$pkgname-$pkgver"
  export GOPATH="$srcdir/gopath"
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export CGO_LDFLAGS="${LDFLAGS}"
  export GOFLAGS="-buildmode=pie -trimpath -ldflags=-linkmode=external -mod=readonly -modcacherw"
  go build -v -o build .
}

package() {
  cd "$pkgname-$pkgver"
  install -Dm755 "build/$pkgname" "$pkgdir/usr/bin/$pkgname"
}
